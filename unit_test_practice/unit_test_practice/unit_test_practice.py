



def test_validate_user_input():
    print( """Testing validate_user_input()...""" )
    tests= [[1, True],
            [0, False],
            [-1, False],
            [True, False],
            ['string', False],
            [0.1, True],
            [111.111, True],
            [0.01, True]]
    for test in tests:
        n = test[0]
        expected = test[1]
        if validate_user_input(n) == expected:
            print('pass')
        else:
            print('failed')
    
    print( """-------------------------------""" )
    return

test_validate_user_input()
