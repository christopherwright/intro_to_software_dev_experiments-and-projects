def get_tax_band(n):
    """
    function returns the tax band code
    for a gross salary, n.
    input n: the salary (int, float)
    returns: str
    """
    # code goes here...
    return "AR"

def get_tax_bands(n):
    if n <= 12500:
        return "PA"
    elif n > 12500 and n <= 50000:
        return "BR"
    elif n > 50000 and n <= 150000:
        return "HR"
    elif n > 150000:
        return "AR"

def test_get_tax_band():
    test = [12499, 49999, 149999,150001,0.1,1000000]
    expected_results = ['PA','BR','HR','AR','PA','AR']
    actual_results = []
    for x in range(len(test)):
        actual_results.append(get_tax_band(float(test[x])))
        if actual_results[x]==expected_results[x]:
            print('Test {}: Pass'.format(x))
        else:
            print('Test {}: Fail'.format(x))
    return
    
        
test_get_tax_band()


       
     
