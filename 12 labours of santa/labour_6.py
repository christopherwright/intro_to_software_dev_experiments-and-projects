""" 
Six: Kill the Six Geese-a-Laying

These murderous birds lived around Lake Stymphalos. Their claws and beaks were sharp as metal and their feathers flew like darts. Santa scared them out of their nests with a rattle and then killed them with the poison arrows he had made from the Dove's blood.

"""

# Help Santa navigate avoid the lake!

# Locations of the nests 
# 1 = nest
# 2 = lake
# 0 = empty
nests = [   [ 0, 1, 0, 1 ],
            [ 0, 2, 2, 0 ],
            [ 1, 2, 2, 1 ],
            [ 0, 1, 0, 1 ] ]

# Write a function which will take a location on the nests grid
# and output a list of possible next moves avoiding the lake
# For example, given the inputs (0, 0) it should return [(0,1), (1,0)]


   



def next_moves(row, col):
    can_move = []
    try: 
            if (nests[row+1][col] >=0) and (nests[row+1][col] <4):
                if (nests[row+1][col] < 2) :
                    can_move.append([row+1, col])

            if (nests[row-1][col] >=0) and (nests[row-1][col] <4):
                if (nests[row-1][col] < 2) :
                    can_move.append([row-1, col])

            if (nests[row][col+1] >=0) and (nests[row][col+1] <4):
                if (nests[row][col+1] < 2): 
                    can_move.append([row, col+1])

            if (nests[row][col-1] >=0) and (nests[row][col-1] <4):
                if (nests[row][col-1] < 2) :
                    can_move.append([row, col-1])
    except:
             pass
    
    

   

    

    
    return print(can_move)
next_moves(2, 3)

