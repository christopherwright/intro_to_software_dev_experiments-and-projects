""" 
Four: Capture the Calling Birds

The people of Mount Erymanthus lived in fear of these deadly birds. Santa chased the wild birds up the mountain and into a snowdrift. He then took them in a net and brought them to King Eurystheus, who was so frightened of the birds that he hid in a huge bronze jar.

"""

# Model a brass jar and add 4 birds to it.

brass_jar = []

def hide_birds(n, brass_jar):
    for i in range(n):
        brass_jar.append('bird'+ str(i+1))
        
    return brass_jar

print( hide_birds(4, brass_jar) )
