""" 
Five: Clean the Five Gold Rings

Thousands of reindeer lived in these rings belonging to King Augeas. They had not been cleaned in 30 years, but Santa was told to clean them completely in a single day. To do so he made two rivers bend so that they flowed into the stables, sweeping out the filth.

"""

# Make the rivers converge to 0 by operating on their data points in a for loop

river_1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

river_2 = [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]


for i in range (0, 10):
  river_1[i] = round((river_1[i] - (1/9*i)), 2)
  river_2[i] = round((river_2[i] + (1/9*i)), 2)
    
    









    

print(river_1, "\n", river_2)
