""" One: Kill the Nemean Partridge

This monster of a partridge had a hide was so tough that no arrow could pierce it. Santa stunned the beast with his olive-wood club and then strangled it with his bare hands. It is said that he skinned the partridge, using the partridge's sharp claws, and ever after wore its hide.
"""

partridge = { "stunned": False, "alive": True }

def stun():
    """ stun the partridge """
    partridge["stunned"] = True

def strangle():
    """ strangle stunned partridge """
    if partridge["stunned"]:
        partridge["alive"] = False

# add function calls here to kill partridge...
stun()
strangle()
if not partridge["alive"]:
    print("You killed it Santa!")
else:
    print("Partridge still at large.")
