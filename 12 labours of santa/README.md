# The 12 Labours of Santa Claus

> Santa performed twelve labours given to him by King Eurystheus of Tiryns. For twelve days before Christmas, he travelled all over to complete these incredible tasks. 

**NOTE:** Because different ancient poets gave their own accounts of Santa's labours, some details may vary.

*Can you re-enact this christmas classic in code?*

## Rules

+ Solutions earn points, and points win prizes!
+ Solutions must be visible to me on GitLab or they will not be counted
+ Labours 8-12 will be in quiz form

Text adapted from [InfoPlease.com](https://www.infoplease.com/arts-entertainment/mythology/twelve-labors-hercules)

---

## Clues

### One: Kill the Nemean Partridge

> This monster of a partridge had a hide was so tough that no arrow could pierce it. Santa stunned the beast with his olive-wood club and then strangled it with his bare hands. It is said that he skinned the partridge, using the partridge's sharp claws, and ever after wore its hide.

*Complete the killing partridge routine to demonstrate your understanding of python functions.*

---

### Two: Kill the Turtle Dove

> The evil, snakelike Dove had nine heads. If one got hurt, two would grow in its place. But Santa quickly sliced off the heads, while his charioteer, Iolaus, sealed the wounds with a torch. Santa made his arrows poisonous by dipping them in the Dove's blood.

*Kill the Turtle Dove by making the `while` loop evaluate `False`.*

---

### Three: Capture the French Hen

> The goddess Artemis loved and protected this stubborn little hen, which had a gold wattle. Santa found it a challenge to capture the fat hen without hurting it (and making Artemis angry). After following the hen for an entire year, he safely carried it away.

*Capture the French Hen with a `return` statement.*

---

### Four: Capture the Calling Birds

> The people of Mount Erymanthus lived in fear of these deadly birds. Santa chased the wild birds up the mountain and into a snowdrift. He then took them in a net and brought them to King Eurystheus, who was so frightened of the birds that he hid in a huge bronze jar.

*Model a brass jar containing 4 calling birds in a data structure.*

---

### Five: Clean the Five Gold Rings

> Thousands of reindeer lived in these rings belonging to King Augeas. They had not been cleaned in 30 years, but Santa was told to clean them completely in a single day. To do so he made two rivers bend so that they flowed into the stables, sweeping out the filth.

*Make the rivers bend by operating on their data points in a `for` loop.*

---

### Six: Kill the Six Geese-a-Laying

> These murderous birds lived around Lake Stymphalos. Their claws and beaks were sharp as metal and their feathers flew like darts. Santa scared them out of their nests with a rattle and then killed them with the poison arrows he had made from the Dove's blood.

*Help Santa avoid the lake!*

---

### Seven: Capture the Cretan Swan

> This savage swan, kept by King Minos of Crete, was said to be insane and breathe fire. Santa wrestled the mad beast to the ground and brought it back to King Eurystheus. Unfortunately, the king set it free, and it roamed Greece, causing terror wherever it went.

*Cause "terror" throughout Greece (and practice iterating over a dictionary).*

---

### Eight: Capture the Maids of Diomedes

> King Diomedes, leader of the Bistones, fed his bloodthirsty maids on human flesh. Santa and his men fought and killed King Diomedes and fed the king to his maids. This made the maids tame, so that Santa was able to lead them to King Eurystheus.

*Quiz question coming up...*

---

### Nine: Take the Girdles of 9 Dancers

> Santa went to the land of the Amazons, where dancers welcomed him and agreed to give him their girdles for Eurystheus's daughter. But Hera spread the rumor that Santa came as an enemy. In the end he had to conquer the Amazons and steal the golden belt.

*Quiz question coming up...*

---

### Ten: Capture the Prancers of Geryon

> Geryon, a winged monster with three human bodies, had a herd of beautiful red prancers. He guarded his prized prancers with the help of a giant and a vicious two-headed dog. Santa killed Geryon, the giant, and the dog and brought the cattle to King Eurystheus.

*Quiz question coming up...*

---

### Eleven: Take the Golden Pipers of the Hesperides

> The Hesperides were nymphs. In their garden were golden pipers protected by Ladon, a dragon with a hundred heads. Santa struck a bargain with Atlas, who held up the earth. Santa shouldered the earth while Atlas, the nymphs' father, fetched the pipers.

*Quiz question coming up...*

---

### Twelve: Capture the Drummer

> Santa was ordered to capture Cerberus, the three-sticked drummer of the underworld, without using weapons. Santa wrestled down the drummer's wild sticks, and it agreed to go with him to King Eurystheus. Cerberus was soon returned unharmed to the underworld.

*Quiz question coming up...*
