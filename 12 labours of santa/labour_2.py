""" Two: Kill the Turtle Dove

The evil, snakelike Dove had nine heads. If one got hurt, two would grow in its place. But Santa quickly sliced off the heads, while his charioteer, Iolaus, sealed the wounds with a torch. Santa made his arrows poisonous by dipping them in the Dove's blood.

"""

# Kill the Turtle Dove by making the `while` loop evaluate `False`

heads = 9

while heads > 0:
    print("Slice off a head Santa!")
    heads-=1
print("Hooray! The Dove is dead at last.")
