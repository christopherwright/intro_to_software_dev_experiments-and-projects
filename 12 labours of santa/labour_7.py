""" 
Seven: Capture the Cretan Swan

This savage swan, kept by King Minos of Crete, was said to be insane and breathe fire. Santa wrestled the mad beast to the ground and brought it back to King Eurystheus. Unfortunately, the king set it free, and it roamed Greece, causing terror wherever it went.

"""

# Cause "terror" throughout Greece

greece = {
        "Thessaloniki": "sunny",
        "Patras": "cheerful",
        "Heraklion": "upbeat",
        "Larissa": "animated",
        "Volos": "calm",
        "Kavala": "jolly",
        "Rhodes": "happy",
        "Athens": "joyful" }

def roam_greece():
    for i in greece:
        greece[i] = "terror"
    print("Roaming Greece...")

roam_greece()

if all( value == "terror" for value in greece.values() ):
    print("Terror throughout Greece")
else:
    print("Cause more terror.")
