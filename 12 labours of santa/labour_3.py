""" Three: Capture the French Hen

The goddess Artemis loved and protected this stubborn little hen, which had a gold wattle. Santa found it a challenge to capture the fat hen without hurting it (and making Artemis angry). After following the hen for an entire year, he safely carried it away.

"""

# Capture the French Hen with a `return` statement.

santas_sack = []

def capture_hen():
    hen = "Cluck cluck!"
    return hen
santas_sack.append( capture_hen() )

print(santas_sack)
