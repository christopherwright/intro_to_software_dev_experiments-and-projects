import requests
import json

#save function writes to file
def save(data):
    with open('saved_props.json', 'w') as fp:
        json.dump(data, fp)


        
 

     

#trump func to pull quotes based on user query

def trump():
    userInput =str(input("enter a trump query: "))
    # make a request to an api
    response = requests.get("https://api.tronalddump.io/search/quote",
        params={"query": userInput })

    # change to json
    data = response.json()
    #catch exception in case user input returns no data
    
    try:
        print("Total quotes: " + str( data["total"]))
        print(data["_embedded"]["quotes"][0]["value"])
    
    except:
        print("no data found")

#pet properties
properties = {"health" : 50, "happiness" : 50}
#pet actions for user to input
actions = ["feed", "stroke"]

play = True
#props print func
def  show_props():
    print(properties)

#game loop
while play == True:
    print(f"balance the health and happiness of your pet type in an actions such as {actions[0]}, {actions[1]}, trump or alternativley save or load you pet.")
    show_props()
    
    userInput =str(input("enter an action: "))
    if properties["health"] == 0 or properties["happiness"] == 0:
        play = False
        print("game over your pet dies")

    elif userInput == "feed":
        properties["health"]+=10
        properties["happiness"]-=10
        show_props()
        print("your pets getting sad :(")
        
    elif userInput == "stroke":
        properties["happiness"]+=10
        properties["health"]-=10
        show_props()
        print("your pets getting hungry :(")

    elif userInput == 'trump':
        trump()
    #save option
    elif userInput == "save":
        save(properties)
        print("saved the pet state")

    elif userInput == "load":
        with open('saved_props.json', 'r') as fp:
            properties = json.load(fp)
        print("loading pet data")
        
    #quit option
    elif userInput == "exit":
        print("goodbye")
        exit()
    # catch all in case nothing valid entered    
    else:
        print("enter an action such as feed or stroke. Or make you pet trump by typing trump or type exit")
        
        